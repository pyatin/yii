<?php

// This is the database connection configuration.
return array(
	'connectionString' => 'mysql:host=localhost;dbname=news_php',
	'emulatePrepare' => true,
    'enableProfiling' => true,
	'username' => 'root',
	'password' => '123',
	'charset' => 'utf8',
);