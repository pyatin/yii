<?php

class NewsController extends Controller
{

    public function actionIndex()
    {
        $articles = Article::model()->findAll(array('limit' => 10));
        
        $this->render('index', array('articles' => $articles));
    }

    
    public function actionView($id)
    {
        $article = Article::model()->findByPk((int) $id);
        
        if(!($article instanceof Article)) {
            throw new CHttpException(404, 'Article not found');
        }
        
        $this->render('view', array('article' => $article));
    }
    
    public function actionByKeyword($id)
    {
        $keyword = Keyword::model()->findByPk((int) $id);
        
        if(!($keyword instanceof Keyword)) {
            throw new CHttpException('Keyword not found');
        }
        
        $this->render('byKeyword', array('keyword' => $keyword, 'articles' => $keyword->articles));
    }

}
