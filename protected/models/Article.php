<?php

/**
 * This is the model class for table "Article".
 *
 * The followings are the available columns in table 'Article':
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $text
 * @property integer $category_id
 * @property integer $user_id
 * @property integer $photo_id
 * @property integer $published
 * 
 * 
 * @property Category $category
 */
class Article extends CActiveRecord
{
    
    public $keywords;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Article';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, text, category_id, keywords', 'required'),
			array('category_id, user_id, photo_id, published', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, date, text, category_id, user_id, photo_id, published', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'tags' => array(self::MANY_MANY, 'Keyword', 'ArticleKeyword(article_id, keyword_id)'),
                //'author'=>array(self::BELONGS_TO, 'User', 'author_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'date' => 'Date',
			'text' => 'Text',
			'category_id' => 'Category',
			'user_id' => 'User',
			'photo_id' => 'Photo',
			'published' => 'Published',
            'keywords' => 'Keywords (comma separated)'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('photo_id',$this->photo_id);
		$criteria->compare('published',$this->published);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function afterSave()
    {
        parent::afterSave();
        
        if(!is_string($this->keywords)) {
            return ;
        }
        
        $keywords = array_unique(explode(',', $this->keywords));
        
        foreach ($keywords as $keyword) {
            
            $name = trim($keyword);
            if($name == '') {
                continue;
            }
            
            $criteria = new CDbCriteria();
            $criteria->addCondition('name = :name');
            $criteria->params['name'] = $name;
            
            $model = Keyword::model()->find($criteria);
            
            if(!($model instanceof Keyword)) {
                $model = new Keyword();
                $model->name = $name;
                $model->save();
            }
            
            $query = 'INSERT INTO ArticleKeyword VALUES (' . $this->id . ', ' . (int) $model->id . ')';
          
            
            Yii::app()->db->createCommand($query)->execute();
        }

    }
    
}
