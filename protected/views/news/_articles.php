<?php

?>

<?php foreach ($articles as /* @var $article Article */ $article) { ?>
<div>
    <a href="<?php echo Yii::app()->urlManager->createUrl('/news/view', array('id' => $article->id)) ?>"><?php echo CHtml::encode($article->title); ?></a>
    <p>
        <?php echo mb_substr($article->text, 0, 100); ?>
    </p>
</div>
<?php } ?>