<?php

/* @var $article Article */

?>

<h1><?php echo CHtml::encode($article->title); ?></h1>

<p><?php echo nl2br(CHtml::encode($article->text)); ?></p>

<?php if($article->category) { ?>
<div>
    Category: <?php echo $article->category->title; ?>
</div>
<?php } ?>
<div>
    User: 
</div>
<?php if($article->tags) { ?>
    Tags: 
    <?php foreach ($article->tags as /* @var $keyword Keyword */ $keyword) { ?>
        <?php echo CHtml::link($keyword->name, array('news/byKeyword', 'id' => $keyword->id)); ?> 
    <?php } ?>
<?php } ?>